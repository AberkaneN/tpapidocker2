FROM java:8-jdk-alpine
COPY target/apidockertp-0.0.1-SNAPSHOT.jar /usr/app/
WORKDIR /usr/app
EXPOSE 8080
ENTRYPOINT ["java","-Dspring.data.mongodb.uri=mongodb://mymongo:27017/dbpersonnages","-Dspring.data.redis.uri=redis://myredis","-Djava.security.egd=file:/dev/./urandom", "-jar", "apidockertp-0.0.1-SNAPSHOT.jar"]