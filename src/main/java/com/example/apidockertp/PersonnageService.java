package com.example.apidockertp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class PersonnageService {
    @Autowired
    private PersonnageRepository personnageRepository;

    @CacheEvict(cacheManager = "redisCacheManager", cacheNames = "personnage", key = "#personnage.prenom", condition = "#personnage.prenom=='Bart'")
    public Personnage save(Personnage personnage) {
        return personnageRepository.save(personnage);
    }

    public List<Personnage> findAll() {
        return personnageRepository.findAll();
    }



}
