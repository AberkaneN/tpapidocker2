package com.example.apidockertp;

import com.example.apidockertp.Personnage;
import com.example.apidockertp.PersonnageRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class DbSeeder implements CommandLineRunner {
    private PersonnageRepository personnageRepository;

    public DbSeeder(PersonnageRepository personnageRepository) {
        this.personnageRepository = personnageRepository;
    }

    @Override
    public void run(String... args) {
        Personnage personnage = new Personnage("Simpson","Homer");
        Personnage personnage1 = new Personnage("Simpson","Lisa");
        Personnage personnage2 = new Personnage("Simpson","Bart");

        //delete all hotels
        this.personnageRepository.deleteAll();

        //add our hotels to the database
        List<Personnage> hotels = Arrays.asList(personnage, personnage1, personnage2);
        this.personnageRepository.saveAll(hotels);

        System.out.println("Initialized database");
    }
}