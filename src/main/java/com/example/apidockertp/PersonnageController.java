package com.example.apidockertp;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/personnages")
public class PersonnageController {
    public static Logger logger = LoggerFactory.getLogger(PersonnageController.class);

    @Autowired
    private PersonnageService service;

    @GetMapping("/add/{nom}/{prenom}")
    public List<Personnage> add(@PathVariable("nom") final String nom,
                                @PathVariable("prenom") final String prenom) {
        Personnage personnage =new Personnage(nom, prenom);
        service.save(personnage);
        return service.findAll();
    }
    @GetMapping
    public String index() {
        return "Hello world" ;
    }

    @GetMapping("/all")
    public List<Personnage> findAll() {
        return service.findAll();
    }
}
