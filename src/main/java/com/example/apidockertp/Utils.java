package com.example.apidockertp;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class Utils {

    private static Map<String, AtomicInteger> DATABASE_ACCESS_COUNTER = new HashMap<>();

    public static void incrementDbAccessFor(String prenom) {
        if (!DATABASE_ACCESS_COUNTER.containsKey(prenom)) {
            DATABASE_ACCESS_COUNTER.put(prenom, new AtomicInteger(0));
        }
        DATABASE_ACCESS_COUNTER.get(prenom).incrementAndGet();
    }

    public static Integer getDbAccessCount(String prenom) {
        return DATABASE_ACCESS_COUNTER.get(prenom).get();
    }
}
