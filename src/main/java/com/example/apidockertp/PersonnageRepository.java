package com.example.apidockertp;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonnageRepository extends MongoRepository<Personnage,String> {
}
