package com.example.apidockertp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.redis.RedisRepositoriesAutoConfiguration;

@SpringBootApplication
public class ApidockertpApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApidockertpApplication.class, args);
    }

}
